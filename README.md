**Documentação do Serviço de Gerenciamento de Produtos**

https://dummyjson.com
API utilizada para gerenciar produtos, permitindo a consulta de produtos existentes e a adição de novos produtos.

-----------------------------------------------------------

GET http://localhost:8080/produtos

POST http://localhost:8080/produtos

Exemplo POST:

{
    "id": 33,
    "title": "Produto Teste",
    "description": "Descrição do produto teste",
    "price": 88.99,
    "discountPercentage": 0.0,
    "rating": 4,
    "stock": 200,
    "brand": "Marca Teste",
    "category": "Categoria Teste",
    "thumbnail": "",
    "images": [""]
}


