package com.example.projetoMonique;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/produtos")
public class ProdutoController {

    private final ProdutoClient produtoClient;
    private final List<Product> produtosLocais;

    public ProdutoController(ProdutoClient produtoClient) {
        this.produtoClient = produtoClient;
        this.produtosLocais = new ArrayList<>();
    }

    @GetMapping
    public ResponseEntity<List<Product>> obterProdutos() {
        ResponseEntity<ProductResponse> responseEntity = produtoClient.obterProdutos();
        if (responseEntity.getStatusCode().is2xxSuccessful()) {
            List<Product> produtosRemotos = responseEntity.getBody().getProducts();
            List<Product> todosProdutos = new ArrayList<>(produtosLocais);
            todosProdutos.addAll(produtosRemotos);
            return ResponseEntity.ok(todosProdutos);
        } else {
            return ResponseEntity.status(responseEntity.getStatusCode()).body(null);
        }
    }

    @PostMapping
    public ResponseEntity<String> adicionarProduto(@RequestBody Product product) {
        produtosLocais.add(product);
        return ResponseEntity.status(HttpStatus.CREATED).body("Produto adicionado com sucesso: " + product.getTitle());
    }
}
