package com.example.projetoMonique;

import com.example.projetoMonique.Product;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import org.springframework.web.client.RestTemplate;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.List;

public class ProductService {

    private final RestTemplate restTemplate;
    private final ObjectMapper objectMapper;

    public ProductService(RestTemplate restTemplate, ObjectMapper objectMapper) {
        this.restTemplate = restTemplate;
        this.objectMapper = objectMapper;
    }

    public List<Product> getProducts() throws JsonProcessingException {
        String url = "https://dummyjson.com/products";
        String response = restTemplate.getForObject(url, String.class);

        List<Product> products = objectMapper.readValue(response, new TypeReference<List<Product>>() {});
        return products;
    }
}
