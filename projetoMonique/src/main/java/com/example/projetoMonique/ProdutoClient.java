package com.example.projetoMonique;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;
import org.springframework.web.client.HttpClientErrorException;

import java.util.Collections;
import java.util.List;

@Component
public class ProdutoClient {

    private final RestTemplate restTemplate;
    private final String apiUrl;
    private final Logger logger = LoggerFactory.getLogger(ProdutoClient.class);

    public ProdutoClient(RestTemplate restTemplate, @Value("${api.url}") String apiUrl) {
        this.restTemplate = restTemplate;
        this.apiUrl = apiUrl;
    }

    public ResponseEntity<ProductResponse> obterProdutos() {
        String url = apiUrl;

        try {
            ResponseEntity<ProductResponse> response = restTemplate.getForEntity(url, ProductResponse.class);
            if (response.getStatusCode().is2xxSuccessful()) {
                return response;
            } else {
                return ResponseEntity.status(response.getStatusCode()).body(new ProductResponse(Collections.emptyList(), 0));
            }
        } catch (HttpClientErrorException ex) {
            logger.error("Erro ao recuperar produtos da API:", ex);
            return ResponseEntity.status(ex.getStatusCode()).body(new ProductResponse(Collections.emptyList(), 0));
        }
    }
}
