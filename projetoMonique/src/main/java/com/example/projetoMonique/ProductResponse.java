package com.example.projetoMonique;

import java.util.List;

public class ProductResponse {
    private List<Product> products;
    private int total;

    public ProductResponse(List<Product> products, int total) {
        this.products = products;
        this.total = total;
    }

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }
}
